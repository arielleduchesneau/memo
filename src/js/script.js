document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDB();

    if (location.pathname.search("fiche-film.html") > 0) {

        var params = (new URL(document.location)).searchParams;

        console.log(params.get("id"));

        connexion.requeteInfoFilm(params.get("id"));
    }
    else {
        connexion.requeteFilmPopulaire();
    }


});

/////////////=======================================================================
class MovieDB {

    constructor() {
        console.log("Parfait 2");

        this.APIKey = "eda01ad95b124c2be1b5f4308d87648f";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurBackdrop = ["300", "780", "1280"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;
    }

/////////////=======================================================================
    requeteFilmPopulaire() {


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));


        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }

/////////////=======================================================================
    retourFilmPopulaire(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheFilmPopulaire(data)


        }

    }

/////////////=======================================================================
    afficheFilmPopulaire(data) {

        console.log(data);
        for (var i = 0; i < this.totalFilm; i++) {


            var unArticle = document.querySelector(".template>.film").cloneNode(true);


//---- changer le titre ---

            unArticle.querySelector("h1").innerText = data[i].title;


//--- changer le texte descriptif ---

            if (data[i].overview === "") {
                unArticle.querySelector(".description").innerText = "Sans description";
            }
            else {
                unArticle.querySelector(".description").innerText = data[i].overview;
            }


//--- changer une image (source, alt et id ) ----

           // unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);
            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w780" + data[i].backdrop_path);
            unArticle.querySelector("img").setAttribute("alt", data[i].title);
            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);


//---- le faire afficher a la bonne place ---

            document.querySelector(".liste-films").appendChild(unArticle);
        }
    }

/////////////=======================================================================
    requeteInfoFilm(movieId) {


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIKey);
        xhr.send();

    }

/////////////=======================================================================
    retourInfoFilm(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            this.afficheInfoFilm(data)

        }

    }

/////////////=======================================================================
    afficheInfoFilm(data) {

        console.log(data);

        document.querySelector("h1").innerText = data.title;

        if (data.overview === "") {
            document.querySelector(".description").innerText = "Sans description";
        }
        else {0
            document.querySelector(".description").innerText = data.overview;
        }


        document.querySelector("img.affiche").setAttribute("src", this.imgPath + "w780" + data.poster_path);
        this.requeteActeur(data.id)
    }


/////////////=======================================================================
    requeteActeur(movieId) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourActeur.bind(this));


        xhr.open("GET", this.baseURL + "movie/" + movieId + "/credits?api_key=" + this.APIKey);

        xhr.send();

    }

/////////////=======================================================================
    retourActeur(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).cast;

            this.afficheActeur(data)
        }
    }

/////////////=======================================================================
    afficheActeur(data) {

        console.log(data);
        for (var i = 0; i < this.totalActeur; i++) {


            var unActeur = document.querySelector(".template>.acteur").cloneNode(true);



            unActeur.querySelector("h3").innerText = data[i].name;


            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w185" + data[i].profile_path);
            unActeur.querySelector("img").setAttribute("alt", data[i].name);


            document.querySelector(".liste-acteurs").appendChild(unActeur);
        }
    }


/////////////=======================================================================
}